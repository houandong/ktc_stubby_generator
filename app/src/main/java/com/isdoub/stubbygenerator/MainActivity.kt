package com.isdoub.stubbygenerator

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.gson.Gson
import com.google.gson.JsonObject
import java.io.File
import java.io.FileWriter
import java.net.URI
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardOpenOption
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        intent.action = Intent.ACTION_SEND
//        intent.putExtra(Intent.EXTRA_TEXT, testData)
//        intent.type = "text/*"


        doGenerate()
    }


    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        doGenerate()
    }


    private fun doGenerate() {
        val permission = android.Manifest.permission.WRITE_EXTERNAL_STORAGE
        if (checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(permission), PERMISSION_REQUEST)
            return
        }


        intent?.let {
            if (it.action == Intent.ACTION_SEND) {
                if (it.type?.startsWith("text") == true) {
                    val txt = it.getStringExtra(Intent.EXTRA_TEXT)
                    Log.e(TAG,"received text: $txt")
                    val jobj = Gson().fromJson(txt, JsonObject::class.java)

                    val data = if (jobj.has("response")) {
                        jobj.get("response")
                    } else {
                        null
                    }
                    if (data==null){
                        Toast.makeText(this, "Empty response body", Toast.LENGTH_SHORT).show()
                        finish()
                        return
                    }
                    Log.w(TAG, "response : $data")
                    val url = if (jobj.has("url")) {
                        val url = jobj.get("url").asString
                        url.substring(url.indexOf("v1"))
                    } else {
                        ""
                    }
                    val fileName = url.replace("v1", "")

                    val subDirPath = fileName.substring(0, fileName.lastIndexOf("/"))
                    val dirAndFileName = OUT_PUT_DIR + subDirPath
                    Log.e(TAG, "dir name : $dirAndFileName")
                    Log.w(TAG, "url: $url")
                    val realFileName = fileName.replace("/", "_") + ".json"

                    Log.e(TAG, "real file name : $dirAndFileName")
                    val dir = File(dirAndFileName)
                    if (!dir.exists()) dir.mkdirs()
                    Log.e(TAG, "dir exist : ${dir.exists()}   ${dir.absolutePath}")
                    val targetFile = File(dir, realFileName.substring(1))
                    if (targetFile.exists()) {
                        Toast.makeText(this, "File existing", Toast.LENGTH_SHORT).show()
                        finish()
                        return
                    }
                    Log.e(TAG, "file path : ${targetFile.absolutePath}")
                    data?.let {
                        val fw = FileWriter(targetFile)
                        fw.write(it.toString())
                        fw.flush()
                    }


                    val yamlFile = "run.yaml"

                    val yaml = File(File(OUT_PUT_DIR), yamlFile)
                    val relativePath = subDirPath + "/" + targetFile.name
                    val yamlData = String.format(Locale.getDefault(), pattern, url, relativePath)

                    Log.e(TAG,"yaml path : ${yaml.absolutePath}")
                    if (!yaml.exists()) {
                        yaml.createNewFile()
                    }

                    val yamFw = FileWriter(yaml,true)

                    yamFw.append(yamlData)
                    yamFw.flush()
                    yamFw.close()

                    Toast.makeText(this, "Generate success...", Toast.LENGTH_SHORT).show()
                    finish()
                }
            }
        }


    }


    fun generate(view: View) {


    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == PERMISSION_REQUEST) {
            doGenerate()
        }
    }

    val testData = """{
  "id": "81",
  "type": "XMLHttpRequest",
  "url": "https://tapktc.pvb4.com/ktcappsit/v1/card/getCardArtworkLastUpdate",
  "status": 200,
  "dataSent": "{\"Form\":[{\"FormHead\":{\"FormId\":\"CC030472I1\"},\"FormData\":{\"ProductId\":[{\"ProductId\":\"VIGI01\"},{\"ProductId\":\"MSWR01\"},{\"ProductId\":\"UPDD01\"},{\"ProductId\":\"VSPP32\"},{\"ProductId\":\"VSPP25\"},{\"ProductId\":\"JCGA72\"},{\"ProductId\":\"VSCA25\"},{\"ProductId\":\"MSCA47\"}]}}]}",
  "responseSize": null,
  "requestHeaders": {
    "accept": "application/json",
    "content-type": "application/json",
    "deviceid": "d4b9e374f4ca337e",
    "os": "android",
    "location": "22.537409,113.947492",
    "lang": "EN",
    "sessionid": "80a5e10c8341b1317092a34bf9594432cb3bf5f038059a3cc0e061e93610437b"
  },
  "responseHeaders": {
    "Strict-Transport-Security": "max-age=31536000 ;",
    "srcbizseqno": "100000000000000001606987407006748",
    "sessionid": "80a5e10c8341b1317092a34bf9594432cb3bf5f038059a3cc0e061e93610437b",
    "retmsgcode": "",
    "retstatus": "N",
    "parentbizseqno": "200000000000000001606987407006748",
    "lastactentryno": "",
    "lang": "EN",
    "location": "22.537409,113.947492",
    "retmessage": "",
    "date": "Thu, 03 Dec 2020 09:23:27 GMT",
    "server": "fasthttp",
    "ip": "10.253.4.0",
    "globalbizseqno": "000000000000000001606987407006748",
    "os": "android",
    "_target_dcn": "",
    "method": "POST",
    "host": "tapktc.pvb4.com",
    "content-length": "538",
    "access-control-allow-headers": "*",
    "deviceid": "d4b9e374f4ca337e",
    "accept-encoding": "gzip",
    "accept": "application/json",
    "httponly": "true",
    "access-control-allow-origin": "*",
    "_dls_element_id": "",
    "trgbizseqno": "1BSCM11scc003047201606987407003147",
    "_dls_element_type": "",
    "_fixed_key_id": "",
    "user-agent": "okhttp/4.2.2",
    "content-type": "application/json; charset=utf-8",
    "secure": "true",
    "_crypto_key_version": "",
    "": "includeSubDomains"
  },
  "responseURL": "https://tapktc.pvb4.com/ktcappsit/v1/card/getCardArtworkLastUpdate",
  "timeout": 0,
  "closeReason": "",
  "messages": "",
  "startTime": 1606987404049,
  "endTime": 1606987407677,
  "method": "POST",
  "response": {
    "Form": [
      {
        "FormHead": {
          "FormId": "CC030472O1"
        },
        "FormData": {
          "ProductId": [
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "JCGA72"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "MSCA47"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "MSWR01"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "UPDD01"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "VIGI01"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "VSCA25"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "VSPP25"
            },
            {
              "LastUpdate": "2029-12-29 00:00:00",
              "ProductId": "VSPP32"
            }
          ]
        }
      }
    ]
  },
  "duration": 3628
}"""


    companion object {
        private const val OUT_PUT_DIR = "/sdcard/stubbyGenerator/output"
        private const val TAG = "Stubby generator"
        const val PERMISSION_REQUEST = 1234
        private const val pattern = """
- request:
    method: POST
    url: /%s
  response:
    status: 200
    file: .%s
"""
    }
}